import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.HomePage;
import pages.SearchResultPage;

import java.util.List;

public class SearchTest {

    private static WebDriver driver;
    private HomePage homePage;
    private SearchResultPage searchResultPage;
    private WebDriverWait webDriverWait;

    @BeforeClass
    public static void mainSetup() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Before
    public void testSetup() {
        driver.get("https://www.rbc.ru/");

        webDriverWait = new WebDriverWait(driver, 10);

        homePage = new HomePage(driver);
        searchResultPage = new SearchResultPage(driver);
    }

    @Test
    public void successSearchTest() {
        searchItemsByQuery("РОС");

        List<WebElement> searchResultItems = searchResultPage.getSearchResultItems();

        Assert.assertTrue(searchResultItems.size() > 0);
        Assert.assertTrue(isEveryItemDisplayed(searchResultItems));

    }

    @Test
    public void noResultSearchTest() {
        searchItemsByQuery("рррр");

        List<WebElement> searchResultItems = searchResultPage.getSearchResultItems();

        Assert.assertEquals(0, searchResultItems.size());

        String emptySearchMessageText = "По вашему запросу ничего не найдено," +
                " вы можете изменить указанные параметры";

        Assert.assertEquals(emptySearchMessageText,
                searchResultPage.getEmptyResultMessage()
                        .getText().replaceAll("\n", " "));
    }

    private void searchItemsByQuery(String query) {
        Assert.assertTrue(homePage.getCurrentUrl().matches("https://www.rbc.ru/"));

        homePage.openSearchArea();
        webDriverWait.until(ExpectedConditions.visibilityOf(homePage.getSearchArea()));

        homePage.addTextToSearchInput(query);
        homePage.submitSearch();

        Assert.assertTrue(searchResultPage.getCurrentUrl()
                .matches("https://www\\.rbc\\.ru/search.+"));

    }

    private boolean isEveryItemDisplayed(List<WebElement> items) {
        return items.stream().allMatch(WebElement::isDisplayed);
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }
}
