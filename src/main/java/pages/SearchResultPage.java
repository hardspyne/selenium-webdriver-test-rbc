package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchResultPage extends BasePage {

    private By searchResultItems = By.xpath("//div[contains(@class,'l-row')]/div[contains(@class,'search-item')]");
    private By emptyResultMessage = By.xpath("//div[@class='g-nofound']");


    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getSearchResultItems() {
        return driver.findElements(searchResultItems);
    }

    public WebElement getEmptyResultMessage() {
        return driver.findElement(emptyResultMessage);
    }
}
