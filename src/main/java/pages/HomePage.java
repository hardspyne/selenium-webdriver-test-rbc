package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage extends BasePage {

    private By searchMenuOpenButton = By.xpath("//span[@class='topline__search__menu__link']");
    private By searchArea = By.xpath("//div[contains(@class,'topline__search__body__inner')]");
    private By searchInput = By.xpath("//input[contains(@class,'topline__search__input')]");
    private By searchSubmitButton = By.xpath("//input[@class='topline__search__button']");

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openSearchArea() {
        driver.findElement(searchMenuOpenButton).click();
    }

    public WebElement getSearchArea() {
        return driver.findElement(searchArea);
    }

    public void addTextToSearchInput(String text) {
        driver.findElement(searchInput).sendKeys(text);
    }

    public void submitSearch() {
        driver.findElement(searchSubmitButton).submit();
    }
}
